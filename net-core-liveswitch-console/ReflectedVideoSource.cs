﻿using System;
using System.Runtime.InteropServices;
using FM.LiveSwitch;
using SkiaSharp;

namespace net_core_liveswitch_console
{
    public class ReflectedVideoSource : CameraSourceBase
    {

        /// <summary>
        /// Gets a label that identifies this class.
        /// </summary>
        public override string Label
        {
            get { return "Reflected Video Source"; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeAudioSource" /> class.
        /// </summary>
        /// <param name="config">The output configuration.</param>
        public ReflectedVideoSource(VideoConfig config)
            : this(config, VideoFormat.Bgr)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeAudioSource" /> class.
        /// </summary>
        /// <param name="config">The output configuration.</param>
        /// <param name="format">The output format.</param>
        public ReflectedVideoSource(VideoConfig config, VideoFormat format)
            : base(format, config)
        { }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        protected override Future<object> DoStart()
        {
            var promise = new Promise<object>();
            promise.Resolve(null);
            return promise;
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        protected override Future<object> DoStop()
        {
            var promise = new Promise<object>();
            promise.Resolve(null);
            return promise;
        }

        public void SendImage(SKBitmap image)
        {
            var config = Config;
            var format = OutputFormat;

            var minimumBufferLength = VideoBuffer.GetMinimumBufferLength(config.Width, config.Height, format.Name);
            var dataBuffer = DataBufferPool.Instance.Take(minimumBufferLength);

            try
            {
                VideoBuffer buffer = new VideoBuffer(config.Width, config.Height, dataBuffer, format);
                CopyTo(image, buffer);

                RaiseFrame(new VideoFrame(buffer));
            }
            catch (Exception ex)
            {
                Log.Error("Could not raise frame.", ex);
            }
            finally
            {
                dataBuffer.Free();
            }
        }

        // To maintain CLS compliance, this method cannot be public (SKImage is not CLS compliant)
        public static void CopyTo(SKBitmap image, VideoBuffer buffer)
        {
            using (var pixels = image.PeekPixels())
            {

                if ((pixels.ColorType == SKColorType.Bgra8888 && buffer.Format.Name == VideoFormat.BgraName) ||
                    (pixels.ColorType == SKColorType.Rgba8888 && buffer.Format.Name == VideoFormat.RgbaName) ||
                    (pixels.ColorType == SKColorType.Rgb888x && buffer.Format.Name == VideoFormat.RgbaName))
                {
                    PixelsToBufferNoCheck(pixels, buffer);
                }
                else
                {
                    throw new Exception(string.Format("Bitmap format does not match buffer format. Bitmap: {0}, Buffer: {1}", pixels.ColorType, buffer.Format.Name));
                }
            }
        }

        internal static void PixelsToBufferNoCheck(SKPixmap pixels, VideoBuffer buffer)
        {
            if (pixels.Width != buffer.Width || pixels.Height != buffer.Height)
            {
                throw new Exception("Bitmap dimensions do not match buffer dimensions.");
            }

            var width = pixels.Width;
            var height = pixels.Height;

            var dstPlane = buffer.DataBuffer;
            var dstStride = buffer.Stride;
            var dstData = dstPlane.Data;

            var bmpDataPtr = pixels.GetPixels();
            var bmpStride = pixels.RowBytes;
            var bmpDataIndex = 0;
            var dstDataIndex = 0;
            for (var y = 0; y < height; y++)
            {
                // @TODO Should there be Marshal.Copy capability in DataBuffer?
                Marshal.Copy(new IntPtr(bmpDataPtr.ToInt64() + bmpDataIndex), dstData, dstDataIndex, dstStride);
                bmpDataIndex += bmpStride;
                dstDataIndex += dstStride;
            }
        }
    }
}
