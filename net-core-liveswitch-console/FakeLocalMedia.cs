﻿using System;
using FM.LiveSwitch;

namespace net_core_liveswitch_console
{
    public class FakeLocalMedia : RtcLocalMedia<object>
    {
        public new FakeAudioSource AudioSource { get { return base.AudioSource as FakeAudioSource; } }

        public new ReflectedVideoSource VideoSource { get { return base.VideoSource as ReflectedVideoSource; } }

        public VideoConfig VideoConfig { get; private set; } = new VideoConfig(320, 240, 15);

        public FakeLocalMedia(VideoConfig videoConfig = null)
            : this(false, false, videoConfig)
        { }

        public FakeLocalMedia(bool disableAudio, bool disableVideo, VideoConfig videoConfig = null)
            : base(disableAudio, disableVideo)
        {
            if (videoConfig != null)
            {
                VideoConfig = videoConfig;
            }
            Initialize();
        }

        public FakeLocalMedia(RtcAudioTrackConfig audioTrackConfig, RtcVideoTrackConfig videoTrackConfig, VideoConfig videoConfig = null)
            : base((audioTrackConfig == null), (videoTrackConfig == null))
        {
            if (videoConfig != null)
            {
                VideoConfig = videoConfig;
            }
            Initialize(audioTrackConfig, videoTrackConfig);
        }

        protected override AudioSink CreateAudioRecorder(AudioFormat inputFormat)
        {
            return new NullAudioSink(inputFormat);
        }

        protected override AudioSource CreateAudioSource(AudioConfig config)
        {
            return new FakeAudioSource(config);
        }

        protected override VideoEncoder CreateH264Encoder()
        {
            return new FM.LiveSwitch.OpenH264.Encoder();
        }

        protected override VideoPipe CreateImageConverter(VideoFormat outputFormat)
        {
            return new FM.LiveSwitch.Yuv.ImageConverter(outputFormat);
        }

        protected override AudioEncoder CreateOpusEncoder(AudioConfig config)
        {
            return new FM.LiveSwitch.Opus.Encoder(config);
        }

        protected override VideoSink CreateVideoRecorder(VideoFormat inputFormat)
        {
            return new NullVideoSink(inputFormat);
        }

        protected override VideoSource CreateVideoSource()
        {
            return new ReflectedVideoSource(VideoConfig, VideoFormat.I420);
        }

        protected override ViewSink<object> CreateViewSink()
        {
            return new NullViewSink<object>(VideoFormat.I420);
        }

        protected override VideoEncoder CreateVp8Encoder()
        {
            return new FM.LiveSwitch.Vp8.Encoder();
        }

        protected override VideoEncoder CreateVp9Encoder()
        {
            return new FM.LiveSwitch.Vp9.Encoder();
        }
    }
}
