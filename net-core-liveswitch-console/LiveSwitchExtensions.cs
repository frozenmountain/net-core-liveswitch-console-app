﻿using FM.LiveSwitch;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace net_core_liveswitch_console
{
    public static class LiveSwitchExtensions
    {
        public static string GenerateRegisterToken(this FM.LiveSwitch.Client client, string secret, string channelId)
        {
            Console.WriteLine("You should NOT generate client register tokens on the client side! Do this on the server!");
            return FM.LiveSwitch.Token.GenerateClientRegisterToken(
                client.ApplicationId,
                client.UserId,
                client.DeviceId,
                client.Id,
                client.Roles,
                new FM.LiveSwitch.ChannelClaim[]
                {
                    new FM.LiveSwitch.ChannelClaim(channelId)
                },
                secret
            );
        }

        private static void BufferToBitmapNoCheck(VideoBuffer buffer, SKBitmap bitmap)
        {
            if (buffer.Width != bitmap.Width || buffer.Height != bitmap.Height)
            {
                throw new Exception("Buffer dimensions do not match bitmap dimensions.");
            }

            var width = buffer.Width;
            var height = buffer.Height;

            var srcPlane = buffer.DataBuffer;
            var srcStride = (buffer.Stride > 0) ? buffer.Stride : width * 3;
            var srcData = srcPlane.Data;

            var bmpDataPtr = bitmap.GetPixels();
            var bmpStride = bitmap.RowBytes;
            var srcDataIndex = srcPlane.Index;
            var bmpDataIndex = 0;
            for (var y = 0; y < height; y++)
            {
                Marshal.Copy(srcData, srcDataIndex, new IntPtr(bmpDataPtr.ToInt64() + bmpDataIndex), srcStride);
                srcDataIndex += srcStride;
                bmpDataIndex += bmpStride;
            }
        }

        public class DownstreamConnectionCreationResult
        {
            public SfuConnection SfuConnection { get; set; }

            public DataStream DataStream { get; set; }

            public VideoTrack RemoteVideoTrack { get; set; }

            public AudioTrack RemoteAudioTrack { get; set; }
        }

        public class UpstreamConnectionCreationResult
        {
            public SfuConnection SfuConnection { get; set; }

            public FakeLocalMedia LocalMedia { get; set; }

        }

        public static DownstreamConnectionCreationResult CreateDownstreamConnection(this FM.LiveSwitch.Channel channel,
            ConnectionInfo remoteConnectionInfo,
            FakeLocalMedia localMedia)
        {
            var imageFormat = VideoFormat.Bgr;
            NullVideoSink imageSink;
            //var remoteMedia = new FM.LiveSwitch.Media.PortableRemoteMedia(false, false, null);
            var remoteAudioTrack = new AudioTrack(new FM.LiveSwitch.Opus.Depacketizer())
                .Next(new FM.LiveSwitch.Matroska.AudioSink("record.mka"));
            var remoteVideoTrack = new VideoTrack(new[]
            {
                new VideoTrack(new FM.LiveSwitch.Vp8.Depacketizer())
                .Next(new[]
                {
                    new VideoTrack(new FM.LiveSwitch.Matroska.VideoSink("record.mkv")),
                    new VideoTrack(new FM.LiveSwitch.Vp8.Decoder())
                    .Next(new FM.LiveSwitch.Yuv.ImageConverter(imageFormat))
                }),
                new VideoTrack(new FM.LiveSwitch.Vp9.Depacketizer())
                .Next(new[]
                {
                    new VideoTrack(new FM.LiveSwitch.Matroska.VideoSink("record.mkv")),
                    new VideoTrack(new FM.LiveSwitch.Vp9.Decoder())
                    .Next(new FM.LiveSwitch.Yuv.ImageConverter(imageFormat))
                }),
                new VideoTrack(new FM.LiveSwitch.H264.Depacketizer())
                .Next(new[]
                {
                    new VideoTrack(new FM.LiveSwitch.Matroska.VideoSink("record.mkv")),
                    new VideoTrack(new FM.LiveSwitch.OpenH264.Decoder())
                    .Next(new FM.LiveSwitch.Yuv.ImageConverter(imageFormat))
                }),
            }).Next(imageSink = new NullVideoSink());

            SKBitmap _bitmap = null;
            imageSink.OnProcessFrame += (frame) =>
            {

                var buffer = frame.GetBuffer(imageFormat);
                if (_bitmap == null || buffer.Width != _bitmap.Width || buffer.Height != _bitmap.Height)
                {
                    _bitmap = new SKBitmap(buffer.Width, buffer.Height);
                }
                BufferToBitmapNoCheck(buffer, _bitmap);

                // here we now have a bitmap ready to go
                localMedia.VideoSource.SendImage(_bitmap);
            };

            AudioStream audioStream = null;
            VideoStream videoStream = null;
            DataStream dataStream = null;
            if (remoteConnectionInfo.HasData && remoteConnectionInfo.DataStream != null && remoteConnectionInfo.DataStream.Channels != null)
            {
                var dataChannels = new List<DataChannel>();
                foreach (var remoteDataChannel in remoteConnectionInfo.DataStream.Channels)
                {
                    var dataChannel = new DataChannel(remoteDataChannel.Label, remoteDataChannel.Ordered, remoteDataChannel.Subprotocol);
                    dataChannels.Add(dataChannel);
                }
                dataStream = new FM.LiveSwitch.DataStream(dataChannels.ToArray());
            }

            if (remoteConnectionInfo.HasAudio)
            {
                //remoteMedia.AudioMuted = false;
                audioStream = new FM.LiveSwitch.AudioStream(null, remoteAudioTrack);
            }
            if (remoteConnectionInfo.HasVideo)
            {
                //remoteMedia.VideoMuted = false;
                videoStream = new FM.LiveSwitch.VideoStream(null, remoteVideoTrack);
            }
            
            var connection = channel.CreateSfuDownstreamConnection(remoteConnectionInfo, audioStream, videoStream, dataStream);
            connection.IceServers = new IceServer[]
            {
                new IceServer("stun:turn.frozenmountain.com:3478"),
                new IceServer("turn:turn.frozenmountain.com:80", "test", "pa55w0rd!"),
                new IceServer("turns:turn.frozenmountain.com:443", "test", "pa55w0rd!")
            };
            

            return new DownstreamConnectionCreationResult()
            {
                DataStream = dataStream,
                SfuConnection = connection,
                RemoteAudioTrack = remoteAudioTrack,
                RemoteVideoTrack = remoteVideoTrack
            };
        }

        public static UpstreamConnectionCreationResult CreateUpstreamConnection(this FM.LiveSwitch.Channel channel)
        {
            var localMedia = new FakeLocalMedia();
            var audioStream = new FM.LiveSwitch.AudioStream(localMedia);
            var videoStream = new FM.LiveSwitch.VideoStream(localMedia);
            
            var connection = channel.CreateSfuUpstreamConnection(audioStream, videoStream);
            connection.IceServers = new IceServer[]
            {
                new IceServer("stun:turn.frozenmountain.com:3478"),
                new IceServer("turn:turn.frozenmountain.com:80", "test", "pa55w0rd!"),
                new IceServer("turns:turn.frozenmountain.com:443", "test", "pa55w0rd!")
            };

            return new UpstreamConnectionCreationResult()
            {
                SfuConnection = connection,
                LocalMedia = localMedia
            };
        }

        public static void SubscribeRemoteParticipants(this FM.LiveSwitch.Channel channel,
            FM.LiveSwitch.Action2<DataStream, ConnectionInfo> arrived, FM.LiveSwitch.Action2<DataStream, ConnectionInfo> left)
        {
            channel.OnRemoteUpstreamConnectionOpen += (ConnectionInfo remoteConnectionInfo) =>
            {
                
                Console.WriteLine("Opening upstream");
                var upstreamResult = channel.CreateUpstreamConnection();
                upstreamResult.SfuConnection.OnStateChange += (ManagedConnection c) =>
                {
                    System.Console.WriteLine("State: " + c.State);
                    if (c.Error != null)
                    {
                        System.Console.WriteLine("Error: " + c.Error.Message + "\n\tException:" + c.Error.Exception);
                    }
                    if (c.IsGoingAway())
                    {
                        upstreamResult.LocalMedia.Stop();
                        System.Console.WriteLine("Upstream connection going away");
                    }
                    else if (c.IsAvailable())
                    {
                        upstreamResult.LocalMedia.Start();
                        System.Console.WriteLine("Upstream connection available");
                    }
                };
                upstreamResult.SfuConnection.Open();

                Console.WriteLine("Opening remote downstream");
                var downstreamResult = channel.CreateDownstreamConnection(remoteConnectionInfo, upstreamResult.LocalMedia);

                // hook up the downstream
                downstreamResult.SfuConnection.OnStateChange += (ManagedConnection c) =>
                {
                    System.Console.WriteLine("State: " + c.State);
                    if (c.Error != null)
                    {
                        System.Console.WriteLine("Error: " + c.Error.Message + "\n\tException:" + c.Error.Exception);
                    }
                    if (c.IsGoingAway())
                    {
                        downstreamResult.RemoteVideoTrack.Destroy();
                        downstreamResult.RemoteAudioTrack.Destroy();
                        System.Console.WriteLine("Downstream connection going away");
                        if (left != null)
                        {
                            left(downstreamResult.DataStream, remoteConnectionInfo);
                        }
                    }
                    else if (c.IsAvailable())
                    {
                        System.Console.WriteLine("Downstream connection available");
                        if (arrived != null)
                        {
                            arrived(downstreamResult.DataStream, remoteConnectionInfo);
                        }
                    }
                };

                downstreamResult.SfuConnection.Open();

                
            };
        }

        public static bool IsGoingAway(this ManagedConnection connection)
        {
            return connection.State == ConnectionState.Closing || connection.State == ConnectionState.Failing;
        }

        public static bool IsAvailable(this ManagedConnection connection)
        {
            return connection.State == ConnectionState.Connected;
        }
    }
}
