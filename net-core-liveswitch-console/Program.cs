﻿using System;

namespace net_core_liveswitch_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var applicationId = "my-app-id";
            var channelId = args.Length > 0 ? args[0] : "987654";
            var secret = "--replaceThisWithYourOwnSharedSecret--";

            FM.LiveSwitch.Log.Provider = new FM.LiveSwitch.ConsoleLogProvider(FM.LiveSwitch.LogLevel.Debug);

            var client = new FM.LiveSwitch.Client("https://demo.liveswitch.fm:8443/sync", applicationId);
            var token = client.GenerateRegisterToken(secret, channelId);
            client.Register(token).Then((channels) =>
            {
                var channel = channels[0];
                channel.SubscribeRemoteParticipants(
                    (dataStream, connectionInfo) =>
                {
                    // arrived
                    //remoteMedia.StartRecording();
                    
                }, 
                    (dataStream, connectionInfo) =>
                {
                    // left
                    //remoteMedia.StopRecording();
                });
            }).Fail(ex =>
            {
                Console.WriteLine(ex.Message);
            });

            while (Console.ReadLine() != "q")
            {
                
                System.Threading.Thread.Sleep(100);
            }

            client.CloseAll();
        }
    }
}
